//
//  InfoViewController.swift
//  MiviCollectionData
//
//  Created by Kiran Juvva on 01/07/18.
//  Copyright © 2018 Kiran Juvva. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet var tableView:UITableView!
    
    @IBOutlet var firstName:UILabel!
    @IBOutlet var lastName:UILabel!
    @IBOutlet var dateOfBirth:UILabel!
    @IBOutlet var emailAddress:UILabel!
    
    @IBOutlet var subscriptionId:UILabel!
    @IBOutlet var subscriptionData:UILabel!
    
    @IBOutlet var productName:UILabel!
    @IBOutlet var productPrice:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Info"
        customerInfoData()  // Read json file
        
    }

    func customerInfoData()  {
        if let path = Bundle.main.path(forResource: "CustomerInfo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = jsonObject as? [String: AnyObject] {
                    let userInfo = dictionary["data"]!["attributes"] as AnyObject
                    // User Info
                    firstName.text = "\(userInfo["first-name"] as AnyObject)"
                    lastName.text = "\(userInfo["last-name"] as AnyObject)"
                    dateOfBirth.text = "\(userInfo["date-of-birth"] as AnyObject)"
                    emailAddress.text = "\(userInfo["email-address"] as AnyObject)"
                    
                    let include = dictionary["included"] as AnyObject!
                    print(include as Any)
                    var type = String()
                    var attributes = [String:AnyObject]() as AnyObject
                    for item:AnyObject! in include as! [AnyObject] {
                        
                        print("Found \(item)")
                        type = item["type"] as! String
                        if type == "subscriptions"
                        {
                            subscriptionId.text = "\(item["id"] as AnyObject)"
                            attributes = item["attributes"] as AnyObject
                            subscriptionData.text = "\(attributes["included-data-balance"] as AnyObject)"
                        }
                        
                        if type == "products"
                        {
                            attributes = item["attributes"] as AnyObject
                            productPrice.text = "\(attributes["price"] as AnyObject)"
                            productName.text = "\(attributes["name"] as AnyObject)"

                        }
                    }                    
                    
                }
                
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
