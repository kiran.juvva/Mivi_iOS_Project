//
//  ViewController.swift
//  MiviCollectionData
//
//  Created by Kiran Juvva on 30/06/18.
//  Copyright © 2018 Kiran Juvva. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    var contents = String()
    @IBOutlet var userNameTextField:UITextField!
    @IBOutlet var passwordTextField:UITextField!
    @IBOutlet var popOverView: UIView!
    
    var timerForPopoverView = Timer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        userNameTextField.delegate=self
        passwordTextField.delegate=self
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if userNameTextField .isEqual(textField)  {
            let maxLength = 55
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else if passwordTextField.isEqual(textField)
        {
            let maxLength = 25
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }else{
            return true
        }
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        var alert:UIAlertController = UIAlertController()
        let isValid :Bool = isValidEmail(testStr: userNameTextField.text!)
        if userNameTextField.text?.count == 0 {
            alert = UIAlertController(title: "Alert!", message: "Enter user email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
            self.present(alert, animated: true)
            return
        }else if !isValid
        {
            alert = UIAlertController(title: "Alert!", message: "Enter valid email-id", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
            self.present(alert, animated: true)
            return
        }else if passwordTextField.text?.count == 0 {
            alert = UIAlertController(title: "Alert!", message: "Enter password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:nil))
            self.present(alert, animated: true)
            return
        }else{
            popOverView.frame = self.view.frame
            self.view.addSubview(popOverView)
            timerForPopoverView = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(timerForPosDeviceView), userInfo: nil, repeats: false)
        }

    }
    
    @objc func timerForPosDeviceView()
    {
        timerForPopoverView.invalidate()
        popOverView.removeFromSuperview()
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController
            {
                if let navigator = self.navigationController
                {
                    navigator.pushViewController(viewController, animated: true)
                }
            }
            
        }, completion: nil)
    }

    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

